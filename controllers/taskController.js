const Task = require('../models/task');

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return task;
		}
	})
}
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(err)
			return false;
		}
		newContent = {
    	"status" : "complete"
}
		result.status = newContent.status;

		return result.save().then((result, saveErr) => {
			if(saveErr){
				console.log(saveErr)
				return false;
			}else{
				return result;
			}
		})
	})
}
module.exports.getTask = (taskId, newContent) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
}
