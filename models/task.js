const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
	status: {
		type: String,
		default: 'pending'
	},
	name: String
});

module.exports = mongoose.model("Task", taskSchema);